// This is just some sample data so you don't have to think of your own!
const items = {
  1: {
    name: "Title of item 1",
    image: "/images/cart-img.jpg",
    desc:  "Description lorem ipsum lorem ipsum lorem ipsum",
    price: 2824
  },

  2: {
    name: "Title of item 2",
    image: "/images/cart-img.jpg",
    desc:  "Description lorem ipsum lorem ipsum lorem ipsum",
    price: 4500
  },

  3: {
    name: "Title of item 3",
    image: "/images/cart-img.jpg",
    desc:  "Description lorem ipsum lorem ipsum lorem ipsum",
    price: 3284
  },

  4: {
    name: "Title of item 4",
    image: "/images/cart-img.jpg",
    desc:  "Description lorem ipsum lorem ipsum lorem ipsum",
    price: 1449
  },

  5: {
    name: "Title of item 5",
    image: "/images/cart-img.jpg",
    desc:  "Description lorem ipsum lorem ipsum lorem ipsum",
    price: 6523
  }
};

export default items;
