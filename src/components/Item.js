import React from "react";
import { getFormattedPrice } from "../helpers";

const Item = ({ item, addToCart, id }) => {
  const { name, price, desc, image } = item;


  const handleAddtoCart = () => {
    addToCart(id);
  };

  return (
    <div className="mb-1 product">
      <div className="product-image"><img src={image} alt="item" /></div>
      <div className="product-info">
      <h3 className="is-size-5 has-text-weight-semi-bold txt-orange product-title">
        {name}
        <span className="has-text-weight-semibold ml-1 is-size-6 ">
          {getFormattedPrice(price)}
        </span>
      </h3>
      <p className="mb-1">{desc}</p>
      <button
        onClick={handleAddtoCart}
        className="button"
      >
        Add to Order
      </button>
      </div>
    </div>
  );
};
export default Item;
