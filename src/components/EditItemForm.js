import React from "react";
import PropTypes from "prop-types";

const EditItemForm = ({ id, item, updateItem, deleteItem }) => {
  const handleOnChange = event => {
    event.preventDefault();
    const updatedItem = {
      ...item
    };
    updatedItem[event.target.name] = event.target.value;
    updateItem(id, updatedItem);
  };

  const onClickDelete = () => {
    deleteItem(id);
  };

  return (
    <div className="mb-1 p-1 box">
      <div className="field is-horizontal">
        <div className="field-body">
          <div className="field">
            <div className="control">
              <input
                className="input is-small"
                type="text"
                name="name"
                placeholder="Name"
                value={item.name}
                onChange={handleOnChange}
              />
            </div>
          </div>
          <div className="field">
            <div className="control">
              <input
                className="input is-small"
                type="text"
                name="price"
                placeholder="Price"
                value={item.price}
                onChange={handleOnChange}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="field">
        <div className="control">
          <input
            className="input is-small"
            name="image"
            placeholder="image"
            value={item.image}
            onChange={handleOnChange}
          />
        </div>
      </div>
      <div className="field">
        <div className="control">
          <textarea
            className="textarea is-small"
            name="desc"
            placeholder="Desc"
            value={item.desc}
            onChange={handleOnChange}
          />
        </div>
      </div>
      <div className="field">
              <button
                type="button"
                className="button btn-delete is-small"
                onClick={onClickDelete}
              >
                Remove Product
              </button>
      </div>
    </div>
  );
};

EditItemForm.propTypes = {
  id: PropTypes.string,
  item: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
    description: PropTypes.string,
    image: PropTypes.string
  }),
  updateItem: PropTypes.func,
  deleteItem: PropTypes.func
};

export default EditItemForm;
