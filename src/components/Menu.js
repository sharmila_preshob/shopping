import React from "react";
import Item from "./Item";
import PropTypes from 'prop-types'

const Menu = ({ items, addToCart }) => (
  <div>
    <ul>
      {Object.keys(items).map(key => (
        <li key={key}>
          <Item item={items[key]} addToCart={addToCart} id={key}/>
        </li>
      ))}
    </ul>
  </div>
);

Menu.propTypes = {
  items: PropTypes.object,
  addToCart: PropTypes.func,
}
export default Menu;
