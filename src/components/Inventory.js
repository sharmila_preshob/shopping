import React from "react";
import PropTypes from "prop-types";
import { isObjectEmpty } from "../helpers";

import Header from "./Header";
import AddItemForm from "./AddItemForm";
import EditItemForm from "./EditItemForm";

const Inventory = ({
  addItem,
  loadDataItems,
  items,
  updateItem,
  deleteItem
}) => (
  <div>
    <Header title="Inventory" />
    {!isObjectEmpty(items) &&
      Object.keys(items).map(key => (
        <EditItemForm
          key={key}
          id={key}
          item={items[key]}
          updateItem={updateItem}
          deleteItem={deleteItem}
        />
      ))}
    <AddItemForm addItem={addItem} />
  </div>
);

Inventory.propTypes = {
  items: PropTypes.object,
  addItem: PropTypes.func,
  deleteItem: PropTypes.func,
  updateItem: PropTypes.func,
  loadDataItems: PropTypes.func
};

export default Inventory;
