import React from "react";
import PropTypes from "prop-types";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { getFormattedPrice } from "../helpers";

import Header from "./Header";

const Order = ({ cart, items, deleteFromCart }) => {
  const transitionOptions = {
    classNames: "order",
    timeout: 250
  };
  const renderCartItem = key => {
    const item = items[key];

    const quantity = cart[key];
    const name = item.name;
    const total = quantity * item.price;

    return (
      <CSSTransition {...transitionOptions} key={key}>
        <div className="level mb-1 border-t pt-1" key={key}>
          <div className="level-left">
            <span className="level-item mr-1 quantity-container">
              <TransitionGroup component={null}>
                <CSSTransition
                  key={quantity}
                  timeout={500}
                  classNames="quantity"
                >
                  <span>{`${quantity} `}</span>
                </CSSTransition>
              </TransitionGroup>
              lbs
            </span>
            <span className="level-item mr-1">{name}</span>
          </div>
          <div className="level-right">
            <span className="level-item">{getFormattedPrice(total)}</span>
            <span
              className="level-item delete is-small"
              onClick={() => deleteFromCart(key)}
            />
          </div>
        </div>
      </CSSTransition>
    );
  };

  const getCartPrice = () => {
    return Object.keys(cart).reduce((total, key) => {
      if (!items[key]) return null;
      const quantity = cart[key];
      const price = items[key].price;
      return total + quantity * price;
    }, 0);
  };

  const isCartEmpty = Object.keys(cart).length === 0;
  const isItemsEmpty = Object.keys(items).length === 0;

  return (
    <div>
      <Header title="Shopping Cart" />
      <ul>
        <li className="is-size-7">
          {!isCartEmpty && !isItemsEmpty && (
            <TransitionGroup component="div" className="cart">
              {Object.keys(cart).map(renderCartItem)}
            </TransitionGroup>
          )}
          {(isCartEmpty || isItemsEmpty) && (
            <CSSTransition
              classNames="fade"
              in={true}
              appear={true}
              timeout={1000}
            >
              <span>You cart is empty</span>
            </CSSTransition>
          )}
        </li>
      </ul>
      <hr />
      {Object.keys(cart).length !== 0 && (
        <h3 className="has-text-weight-semibold mr-1 is-size-6 is-pulled-right">
          <span className="pr-1">Total</span> {getFormattedPrice(getCartPrice())}
        </h3>
      )}
    </div>
  );
};

Order.propTypes = {
  cart: PropTypes.object,
  items: PropTypes.object,
  deleteFromCart: PropTypes.func,
}

export default Order;
