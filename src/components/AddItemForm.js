import React, { Component } from "react";
import PropTypes from 'prop-types'

class AddItemForm extends Component {
  static propTypes = {
    addItem: PropTypes.func
  }

  name = React.createRef();
  price = React.createRef();
  desc = React.createRef();
  image = React.createRef();

  handleOnSubmit = event => {
    event.preventDefault();
    const item = {
      name: this.name.current.value,
      price: this.price.current.value,
      desc: this.desc.current.value,
      image: this.image.current.value
    };
    this.props.addItem(item);
    event.target.reset();
  };

  render() {
    return (
      <form className="mb-2" onSubmit={this.handleOnSubmit}>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <div className="control">
                <input
                  className="input is-small"
                  type="text"
                  name="name"
                  placeholder="Title"
                  ref={this.name}
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <input
                  className="input is-small"
                  type="text"
                  name="price"
                  placeholder="Price"
                  ref={this.price}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="field">
          <div className="control">
            <input
              className="input is-small"
              name="image"
              placeholder="Image url"
              ref={this.image}
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <textarea
              className="textarea is-small"
              name="desc"
              placeholder="Desc"
              ref={this.desc}
            />
          </div>
        </div>
        <div className="field">
                <button type="submit" className="button btn-add is-small">
                  Add Product
                </button>
        </div>
      </form>
    );
  }
}

export default AddItemForm;
