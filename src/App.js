import React, { Component } from "react";
import Proptypes from "prop-types";
import data from "./data/data.js";


import Header from "./components/Header";
import Menu from "./components/Menu";
import Order from "./components/Order";
import Inventory from "./components/Inventory";

class App extends Component {
  static propTypes = {
    match: Proptypes.object
  };

  state = {
    items: data,
    cart: {},
    isLoading: false
  };

  addToCart = id => {
    const cart = { ...this.state.cart };
    cart[id] = cart[id] + 1 || 1;
    this.setState({ cart });
  };

  addItem = item => {
    const items = { ...this.state.items };
    items[`item-${Date.now()}`] = item;
    this.setState({ items });
  };

  updateItem = (id, item) => {
    const items = { ...this.state.items };
    items[id] = item;
    this.setState({ items });
  };

  deleteItem = key => {
    const items =  this.state.items;
    const { [key]: _, ...newItems } = items;
    this.setState({ items: newItems });
  };

  deleteFromCart = key => {
    const cart = { ...this.state.cart };
    delete cart[key];
    this.setState({ cart });
  };

  loadDataItems = () => {
    this.setState({
      items: data
    });
  };

  render() {
    const { items, cart, isLoading } = this.state;
    return (
      <div className="columns is-gapless">
        <div className="column">
          <div className="box is-fullheight">
            <Header title="List of Products" />
            {isLoading && (
              <span className="icon is-medium">
                <i className="fas fa-spinner fa-pulse fa-2x" />
              </span>
            )}
            {!isLoading && <Menu items={items} addToCart={this.addToCart} />}
          </div>
        </div>
        <div className="column">
          <div className="box is-fullheight">
            <Order
              cart={cart}
              items={items}
              deleteFromCart={this.deleteFromCart}
            />
          </div>
        </div>
        <div className="column">
          <div className="box is-fullheight">
            <Inventory
              loadDataItems={this.loadDataItems}
              addItem={this.addItem}
              items={items}
              updateItem={this.updateItem}
              deleteItem={this.deleteItem}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default App;
